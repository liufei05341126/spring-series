package com.javacode2018.mybatis.module2.mapper;

import com.javacode2018.mybatis.module2.model.User2Model;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 公众号：路人甲Java，工作10年的前阿里P7分享Java、算法、数据库方面的技术干货！
 * <a href="http://www.itsoku.com">个人博客</a>
 */
@Mapper
public interface User2Mapper {
    void insert(User2Model userModel);

    List<User2Model> getList();
}
